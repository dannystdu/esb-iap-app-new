package tools;

public class ReceiptDataVerifyRequest {
	
	private long OrderId;
	private String PackageName;
	private String ProductItemId;
	private String Platform;
	private String Environment;
	private boolean isSubscription;
	private String  PurchaseToken;
	private String  UserID;
	
	
	public long getOrderId() {
		return OrderId;
	}


	public void setOrderId(long orderId) {
		OrderId = orderId;
	}


	public String getPackageName() {
		return PackageName;
	}


	public void setPackageName(String packageName) {
		PackageName = packageName;
	}


	public String getProductItemId() {
		return ProductItemId;
	}


	public void setProductItemId(String productItemId) {
		ProductItemId = productItemId;
	}


	public String getPlatform() {
		return Platform;
	}


	public void setPlatform(String platform) {
		Platform = platform;
	}


	public String getEnvironment() {
		return Environment;
	}


	public void setEnvironment(String environment) {
		Environment = environment;
	}


	public boolean getIsSubscription() {
		return isSubscription;
	}


	public void setIsSubscription(boolean isSubscription) {
		this.isSubscription = isSubscription;
	}


	public String getPurchaseToken() {
		return PurchaseToken;
	}


	public void setPurchaseToken(String purchaseToken) {
		PurchaseToken = purchaseToken;
	}


	public String getUserID() {
		return UserID;
	}


	public void setUserID(String userID) {
		UserID = userID;
	}


	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
